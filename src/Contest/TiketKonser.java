package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...
    protected String nama;
    protected double harga;
    /*atribut "nama" dan "harga" dideklarasikan dengan "protected" berarti hanya
    dapat diakses oleh class yang sama, subclass, dan class di dalam paket yang sama.*/

    public TiketKonser(String nama, double harga) {
        this.nama = nama;
        this.harga = harga;
    }
    /*constructor "TiketKonser" memiliki dua parameter, yaitu "nama"
    dan "harga". saat objek "TiketKonser" dibuat, constructor ini
    digunakan untuk menginisialisasi nilai atribut "nama" dan "harga"
    sesuai dengan nilai yang diberikan melalui parameter constructor.
     */

    public String getNama() {
        return nama;
    }
    /*method "getNama()" digunakan untuk mengembalikan nilai atribut "nama".
    method ini memiliki modifier akses "public", makanya bisa diakses dari kelas manapun.*/

    public abstract double HitungHarga();
}
/*method "HitungHarga()" dideklarasikan sebagai metode abstrak dengan kata kunci "abstract".
method ini tidak memiliki implementasi di kelas "TiketKonser" karena merupakan method abstrak.
class yang mewarisi kelas "TiketKonser" harus mengimplementasikan metode ini sesuai dengan kebutuhan.
 */