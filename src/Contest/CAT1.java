package Contest;

class CAT1 extends TiketKonser {
    //Do your magic here...
    public CAT1(String nama, double harga) {
        /*class "CAT1" memiliki constructor yang terdapat dua parameter yaitu "nama"
        yang merupakan nama tiket konser dan "harga" yang merupakan harga tiket konser.*/
        super(nama, harga);
        /*constructor tersebut memanggil construkcor dari class induk ("TiketKonser")
        menggunakan kata kunci "super" untuk menginisialisasi nilai "nama" dan "harga" dari tiket konser.*/
    }

    @Override //untuk mewarisi method
    public double HitungHarga() {
        return harga;
    }
}
/*method "HitungHarga()" yang diwarisi dari class "TiketKonser" yang berfungsi untuk mengembalikan nilai harga
tiket konser yang telah diinisialisasi.*/