package Contest;

class VVIP extends TiketKonser {
    // Do your magic here...
    public VVIP(String nama, double harga) {
        /* class "VVIP" memiliki constructor yang terdapat dua parameter "nama"
        yang merupakan nama tiket konser dan "harga" yang merupakan harga tiket konser.*/
        super(nama, harga);
        /*constructor tersebut memanggil constructor dari class induk ("TiketKonser")
        menggunakan kata kunci "super" untuk menginisialisasi nilai "nama" dan "harga" dari tiket konser.*/
    }

    @Override //untuk mewarisi method
    public double HitungHarga() {
        return harga;
    }
}
/*method "HitungHarga()" yang diwarisi dari class "TiketKonser" yang berfungsi untuk mengembalikan nilai harga
tiket konser yang telah diinisialisasi.*/