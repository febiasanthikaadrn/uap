package Contest;

class PemesananTiket {
    // Do your magic here...
    private static TiketKonser[] tiketK;

    static {
        /*objek-objek dibawah dibuat menggunakan constructor dari kelas-kelas turunan "TiketKonser",
        yaitu "CAT8", "CAT1", "FESTIVAL", "VIP", dan "VVIP". setiap objek ini memiliki nama tiket
        dan harga tiket yang berbeda-beda.
         */
        tiketK = new TiketKonser[]{
                new CAT8("CAT 8", 1000000),
                new CAT1("CAT 1", 3000000),
                new FESTIVAL("FESTIVAL", 6000000),
                new VIP("VIP", 9000000),
                new VVIP("UNLIMITED EXPERIENCE", 12000000)
        };
    }

    /*atribut array "tiketK" yang dideklarasikan sebagai "private static TiketKonser[]".
    berarti memiliki sebuah array TiketKonser" yang dapat diakses secara privat oleh
    kelas itu sendiri dan bersifat static, berarti hanya ada satu salinan array yang
    terhubung dengan kelas tersebut.
     */

    public static TiketKonser pilihTiket(int x) {
        return tiketK[x];
    }
}
/*method "pilihTiket(int x)" yang mengambil x sebagai argumen.
method ini mengembalikan objek tiket dari array "tiketK" berdasarkan indeks yang diberikan.
method ini berguna untuk memilih tiket berdasarkan indeks tertentu dari array yang telah diinisialisasi sebelumnya.*/
