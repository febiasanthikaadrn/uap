/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {
    public static void main(String[] args) {
        //Do your magic here...
        Scanner scanner = new Scanner(System.in);

        System.out.println("-- Selamat datang di Pemesanan Tiket Coldplay! --");

        try { //try-catch untuk menangani exception yang mungkin terjadi selama proses pemesanan tiket.
            System.out.print("Masukkan nama pemesan: ");
            String NamaP = scanner.nextLine();

            if (NamaP.length() > 15) {
                /*ngecek apakah panjang nama pemesan lebih dari 15 karakter atau tidak.
                kalau lebih, lemparkan InvalidInputException dengan pesan kesalahan yang sesuai.*/
                throw new InvalidInputException("Panjang nama pemesan max 15 huruf!");
            }

            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan: ");

            int pilih;
            try {
                pilih = Integer.parseInt(scanner.nextLine());
                /*mengonversi input pengguna menjadi tipe data int menggunakan Integer.parseInt()
                dan menangkap NumberFormatException jika input tidak valid.*/
            } catch (NumberFormatException e) {
                /*NumberFormatException merupakan sebuah exception yang dilempar oleh metode parseInt()
                jika string yang diubah tidak valid sebagai bilangan bulat.*/
                throw new InvalidInputException("Input tidak valid! Input pilihan tiket dengan angka.");
            } /*jika terjadi NumberFormatException, blok catch akan dieksekusi. di dalamnya dibangkitkan
              objek InvalidInputException dengan pesan kesalahan yang sesuai.*/

            if (pilih < 1 || pilih > 5) {
                /*ngecek apakah pilihan tiket yang dimasukkan pengguna valid (antara 1-5).
                jika tidak valid, lemparkan InvalidInputException dengan pesan kesalahan yang sesuai.*/
                throw new InvalidInputException("Input tidak valid! Angka yang dapat dipilih hanya 1-5.");
            }

            TiketKonser tiket = PemesananTiket.pilihTiket(pilih - 1);
            //menggunakan method PemesananTiket.pilihTiket() untuk memilih objek tiket berdasarkan pilihan pengguna.
            /*baris ini memanggil method pilihTiket() dari kelas PemesananTiket dan menyimpan objek tiket yang dipilih
            ke dalam variabel tiket. method pilihTiket() menerima argumen pilih - 1, yang merupakan indeks tiket yang
            dipilih oleh pengguna yang dikurangi 1 (karena indeks array dimulai dari 0). Method ini mengembalikan objek
            tiket yang sesuai dengan pilihan. */

            String KodeBooking = generateKodeBooking();
            //menggunakan method generateKodeBooking() untuk menghasilkan kode booking.
            /*baris ini memanggil method generateKodeBooking() dan menyimpan kode booking yang dihasilkan ke dalam
            variabel KodeBooking. method generateKodeBooking() menghasilkan kode booking secara acak atau sesuai dengan
            aturan yang ditentukan dalam method tersebut. kode booking ini akan digunakan untuk mengidentifikasi pemesanan
            tiket secara unik/acak.*/


            String tglPesanan = getCurrentDate();
            //menggunakan metode getCurrentDate() untuk mendapatkan tanggal pesanan.
            /*baris ini memanggil method getCurrentDate() dan menyimpan tanggal pesanan saat ini ke dalam variabel tglPesanan.
            method getCurrentDate() mengembalikan tanggal saat ini dalam format yang ditentukan, misalnya dalam format
            tanggal-bulan-tahun dll.*/

            //menampilkan detail pemesanan kepada pengguna, termasuk nama pemesan, kode booking, tanggal pesanan, tiket yang dipesan, dan total harga tiket.
            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + NamaP);
            System.out.println("Kode Booking: " + KodeBooking);
            System.out.println("Tanggal Pesanan: " + tglPesanan);
            System.out.println("Tiket yang dipesan: " + tiket.getNama());
            System.out.println("Total harga: " + tiket.HitungHarga() + " USD");
        } catch (InvalidInputException e) {
            System.out.println(e.getMessage());
            //jika terjadi InvalidInputException, pesan kesalahan akan ditampilkan menggunakan e.getMessage().
        } catch (Exception e) {
            System.out.println(e.getMessage());
            //jika terjadi exception lainnya, pesan kesalahan umum akan ditampilkan menggunakan e.getMessage().
        } finally {
            scanner.close();
            //menggunakan finally untuk memastikan bahwa objek Scanner ditutup dengan memanggil scanner.close().
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}